package com.example.jeju;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class JejuActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_activity_jeju);
		
		Button startBtn = (Button)findViewById(R.id.btn_start_game);
		  startBtn.setOnClickListener(new View.OnClickListener() {
		   
		   @Override
		   public void onClick(View arg0) {
		    // TODO Auto-generated method stub
			   Intent intent = new Intent(getApplicationContext(), SettingActivity.class);
			    startActivity(intent);

		   }
		  });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.jeju, menu);
		return true;
	}

}
